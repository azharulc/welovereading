<div class="listing_page">
	<h2><?php echo $title ?></h2>
	<div class="listing_container">
		<h3><?php echo $subtitle ?></h3>
		<ul class="listing">
			<?php if (isset($data_var) && !empty($book_list->{$data_var})) : ?>
				<?php foreach ($book_list->{$data_var} as $book_info) : ?>
					<?php
						if (!empty($book_info->cover_edition_key)) {
							$cover_url = 'http://covers.openlibrary.org/b/olid/' . $book_info->cover_edition_key . '-M.jpg';
							$book_id = $book_info->cover_edition_key;
						} else {
							$cover_url = '/assets/img/no-preview.png';
							$book_id = $book_info->edition_key[0];
						}
					?>
					<li>
						<a href="<?php echo site_url('book_information') ?>?book=<?php echo $book_id ?>">
							<img src="<?php echo $cover_url ?>" alt="<?php echo $book_info->title ?>" width="180" height="275" />
							<p><?php echo $book_info->title ?></p>
						</a>
					</li>
				<?php endforeach ?>
			<?php elseif (!empty($recommendations)) : ?>
				<?php foreach ($recommendations as $key => $value) : ?>
					<?php
						$book_json = book_api::load_book($key);
						$book = json_decode($book_json);
						foreach ($book as $info) {
							//var_dump($info);
						if (!empty($info->cover->medium)) {
							$cover_url = $info->cover->medium;
							$book_id = $key;
						} else {
							$cover_url = '/assets/img/no-preview.png';
							$book_id = $key;
						}
					?>
					<li>
						<a href="<?php echo site_url('book_information') ?>?book=<?php echo $key ?>">
							<img src="<?php echo $cover_url ?>" alt="<?php echo $info->title ?>" width="100%" height="80%" />
							<p><?php echo $info->title ?></p>
						</a>
					</li>
					<?php } ?>
				<?php endforeach ?>
			<?php endif ?>
		</ul>
	</div>
</div>
<?php //var_dump($book_list) ?>