<?php if (validation_errors() != false) : ?>
	<div class="val_errors">
		<?php echo validation_errors(); ?>
	</div>
<?php endif; ?>
<?php if (!empty($error) && $error == 'username_exists') : ?>
	<div class="val_errors">
		<p>The username entered already exists, please try again.</p>
	</div>
<?php elseif (!empty($error) && $error == 'email_exists') : ?>
	<div class="val_errors">
		<p>The email already exists, please try again.</p>
	</div>
<?php endif; ?>
<div class="registration_wrapper">
	<div class="registration_controller">
		<h2>Registration</h2>
		<form action="<?php echo site_url('registration/register') ?>" method="post">
			<div class="col col_1">
				<div class="field">
					<label for="firstname">First Name</label>
					<input type="text" required name="firstname" id="firstname" placeholder="First name" value="<?php echo set_value('firstname') ?>" />
					<div class="clearfix"></div>
				</div>
				<div class="field">
					<label for="lastname">Last Name</label>
					<input type="lastname" required name="lastname" id="lastname" placeholder="Last name" value="<?php echo set_value('lastname') ?>" />
					<div class="clearfix"></div>
				</div>
				<div class="field">
					<label for="email">Email</label>
					<input type="email" required name="email" id="email" placeholder="Enter a valid email" value="<?php echo set_value('email') ?>" />
					<div class="clearfix"></div>
				</div>
				<div class="field">
					<label for="fave_gen">Favourite Genre</label>
					<input type="text" required name="fave_gen" id="fave_gen" placeholder="Genre" value="<?php echo set_value('fave_gen') ?>" />
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col col_2">
				<div class="field">
					<label for="username">Username</label>
					<input type="text" required name="username" placeholder="Username" id="username" value="<?php echo set_value('username') ?>" />
					<div class="clearfix"></div>
				</div>
				<div class="field">
					<label for="password">Password</label>
					<input type="password" required name="password" placeholder="Password" id="pasword" />
					<div class="clearfix"></div>
				</div>
				<div class="field">
					<label for="conf_password">Confirm Password</label>
					<input type="password" required name="conf_password" placeholder="Confirm Password" id="conf_password" />
					<div class="clearfix"></div>
				</div>
				<div class="button">
					<input class="button" type="submit" value="Register" />
					<div class="clearfix"></div>
				</div>
			</div>
		</form>
		<div class="clearfix"></div>
	</div>
</div>