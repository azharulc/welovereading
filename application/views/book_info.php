<?php if (validation_errors() != false) : ?>
	<div class="val_errors book_info_val_error">
		<?php echo validation_errors(); ?>
	</div>
<?php endif; ?>
<div class="info_wrapper">
	<h2>Book Information</h2>
	<?php foreach ($book_info as $details) : ?>
		<form action="<?php echo site_url('book_information/save') ?>" method="post">
			<?php
				if (!empty($details->subjects))  :
					$subject = '';
					foreach ($details->subjects as $sub) {
						$subject .=  $sub->name . ', ';
					}
			?>
			<input type="hidden" name="subject" value="<?php echo $subject ?>">
			<?php endif ?>
			<input type="hidden" name="book_id" value="<?php echo $book_id ?>">
			<?php if (!empty($details->cover->large)) : ?>
				<img src="<?php echo $details->cover->large ?>" alt="<?php echo $details->title ?>" width="370" height="" />
				<input type="hidden" name="img" value="<?php echo $details->cover->large ?>">
			<?php else : ?>
				<img src="/assets/img/no-preview.png" alt="<?php echo $details->title ?>" width="370" height="" />
				<input type="hidden" name="img" value="/assets/img/no-preview.png">
			<?php endif; ?>
			<div class="book_info">
				<h2><?php echo $details->title ?></h2>
				<input type="hidden" name="title" value="<?php echo $details->title ?>">
				<h3><?php echo $details->authors[0]->name ?></h3>
				<input type="hidden" name="author" value="<?php echo $details->authors[0]->name ?>">
				<?php if (!empty($details->subtitle)) : ?>
					<h4>"<?php echo $details->subtitle ?>"</h4>
					<input type="hidden" name="subtitle" value="<?php echo $details->subtitle ?>">
				<?php endif; ?>
				<?php if ($this->session->userdata('is_loggedin') == true) : ?>
					<?php if (!$has_read_book) : ?>
						<p>A rating is required in order to add the book to your list.</p>
						<div class="stars">
							<input name="stars" type="radio" class="star" value="1" required />
							<input name="stars" type="radio" class="star" value="2" required />
							<input name="stars" type="radio" class="star" value="3" required />
							<input name="stars" type="radio" class="star" value="4" required />
							<input name="stars" type="radio" class="star" value="5" required />
						</div>
						<div class="clearfix"/></div>
						<input class="button" type="submit" value="Add to List" />
					<?php endif ?>
				<?php endif ?>
			</div>
		</form>
		<div class="clearfix"></div>
		<div class="review_area">
			<ul class="nav nav-tabs">
				<li><a href="#review">Reviews</a></li>
				<?php if ($this->session->userdata('is_loggedin') && $has_read_book) : ?>
					<li><a href="#leave_a_review">Leave a Review</a></li>
				<?php endif ?>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="review">
					<h3>Reviews</h3>
					<?php if (empty($reviews)) : ?>
						<p>No Reviews.</p>
						<div class="clearfix"></div>
					<?php endif ?>
					<?php foreach ($reviews as $review) : ?>
					<div class="review_row">
						<div class="top">
							<p class="username"><?php echo $review->username ?></p>
							<div class="stars">
								<input name="star3<?php echo $review->username ?>" type="radio" class="star" disabled="disabled" <?php echo $review->rating == 1 ? 'checked="checked"' : '' ?>/>
								<input name="star3<?php echo $review->username ?>" type="radio" class="star" disabled="disabled" <?php echo $review->rating == 2 ? 'checked="checked"' : '' ?>/>
								<input name="star3<?php echo $review->username ?>" type="radio" class="star" disabled="disabled" <?php echo $review->rating == 3 ? 'checked="checked"' : '' ?>/>
								<input name="star3<?php echo $review->username ?>" type="radio" class="star" disabled="disabled" <?php echo $review->rating == 4 ? 'checked="checked"' : '' ?>/>
								<input name="star3<?php echo $review->username ?>" type="radio" class="star" disabled="disabled" <?php echo $review->rating == 5 ? 'checked="checked"' : '' ?>/>
							</div>
							<div class="clearfix"/></div>
						</div>
						<div class="middle">
							<p class="user_title"><?php echo $review->title ?></p>
							<p class="user_review"><?php echo $review->review ?></p>
						</div>
					</div>
					<?php endforeach ?>
				</div>
				<div class="tab-pane" id="leave_a_review">
					<h3>Leave a Review</h3>
					<form action="book_information/leave_review" method="post">
						<div class="field">
							<label for="review_title">Review Title</label>
							<input type="text" required id="review_title" class="review_title" name="review_title" <?php echo !empty($user_review) ? 'value="' . $user_review[0]->title . '"' : '' ?> />
						</div>
						<div class="clearfix"></div>
						<div class="field">
							<label for="review_body_box">Review Body</label>
							<textarea name="review_body_box" required id="review_body_box" class="review_body_box"><?php echo !empty($user_review) ? $user_review[0]->review : '' ?></textarea>
						</div>
						<div class="clearfix"></div>
						<input type="hidden" name="book_id" value="<?php echo $this->input->get('book') ?>" />
						<input type="submit" class="button"/>
					</form>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	<?php endforeach ?>
</div>