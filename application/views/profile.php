<div class="profile_wrapper">
	<h2><?php echo $title ?></h2>
	<div class="profile_info col col1">
		<p class="title">Username:</p>
		<p class="value"><?php echo $username ?></p>
		<div class="clearfix"></div>
		<p class="title">First name:</p>
		<p class="value"><?php echo $firstname ?></p>
		<div class="clearfix"></div>
		<p class="title">Last name:</p>
		<p class="value"><?php echo $lastname ?></p>
		<div class="clearfix"></div>
	</div>
	<div class="recommendations_col col col2">
		<p>For an accurate reccomendation, please ensure you have made a list of books you have read and given them ratings!</p>
		<div class="clearfix"></div>
		<a class="button" href="<?php echo site_url('profile/recommendations') ?>">Get Recommendations!</a>
	</div>
	<div class="clearfix"></div>
	<div class="read_list">
		<h3>Read List</h3>
		<?php foreach ($user_books as $book) : ?>
			<div class="book_display">
				<div class="image_container">
					<img src="<?php echo str_replace('L.jpg', 'M.jpg', $book->image) ?>" alt="<?php echo $book->title ?>" width="180" height="" />
				</div>
				<div class="book_info_container">
					<p class="book_title"><?php echo $book->title ?></p>
					<p class="book_author"><?php echo $book->author ?></p>
				</div>
				<div class="stars">
					<form action="<?php echo site_url('profile/edit_rating') ?>" method="post">
						<input name="stars-<?php echo $book->book_id ?>" type="radio" class="star" value="1" <?php echo ($book->rating == 1) ? 'checked="checked"' : '' ?> />
						<input name="stars-<?php echo $book->book_id ?>" type="radio" class="star" value="2" <?php echo ($book->rating == 2) ? 'checked="checked"' : '' ?> />
						<input name="stars-<?php echo $book->book_id ?>" type="radio" class="star" value="3" <?php echo ($book->rating == 3) ? 'checked="checked"' : '' ?> />
						<input name="stars-<?php echo $book->book_id ?>" type="radio" class="star" value="4" <?php echo ($book->rating == 4) ? 'checked="checked"' : '' ?> />
						<input name="stars-<?php echo $book->book_id ?>" type="radio" class="star" value="5" <?php echo ($book->rating == 5) ? 'checked="checked"' : '' ?> />
						<input type="hidden" class="book_id" name="book_id" value="<?php echo $book->book_id ?>" />
						<input class="button" type="submit" />
					</form>
				</div>
				<div class="to_do">
					<a class="view" href="<?php echo site_url('book_information') . '?book=' . $book->book_id ?>">View</a> | <a class="remove" href="<?php echo site_url('profile/remove_from_list') . '?book=' . $book->book_id ?>">Remove</a>
				</div>
				<div class="clearfix"></div>
			</div>
		<?php endforeach ?>
	</div>
</div>