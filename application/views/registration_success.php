<div class="registration_wrapper success">
	<h2>Success!</h2>
	<p>You have successfully registered, <a href="<?php echo site_url('login'); ?>">click here</a> to login.</p>
</div>