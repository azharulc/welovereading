<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Loads header and footer together
$this->load->view('global/header');
echo $content;
$this->load->view('global/footer');