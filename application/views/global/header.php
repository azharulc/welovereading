<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title><?php echo !empty($title) ? $title : '' ?> | We Love Reading</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="/assets/css/normalize.css">
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/jquery.rating.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <div class="page_container">
            <!--[if lt IE 7]>
                <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
            <![endif]-->
                <header>
                    <div class="header_wrap">
                        <div class="head_wrap">
                            <h1>
                                <a class="ir" href="/">
                                    We Love Reading
                                </a>
                            </h1>
                        </div>
                    </div>
                    <div class="head_wrap">
                        <a class="show_menu" href="#">Menu</a>
                        <nav>
                            <a class="home" href="/">Home</a>
                            <?php if ($this->session->userdata('is_loggedin')) : ?>
                                <a href="<?php echo site_url('profile') ?>">Profile</a>
                                <a href="<?php echo site_url('/profile/recommendations') ?>">Recommendations</a>
                            <?php endif; ?>
                            <?php if (!$this->session->userdata('is_loggedin')) : ?>
                                <a class="logout" href="<?php echo site_url('login'); ?>">Login</a>
                            <?php else : ?>
                                <a class="logout" href="<?php echo site_url('login/logout'); ?>">Logout</a>
                            <?php endif; ?>
                            <div class="clearfix"></div>
                        </nav>
                    </div>
                    <div class="clearfix"></div>
                    <div class="search_wrap">
                        <form action="<?php echo site_url('main/search') ?>" method="get">
                            <input type="text" name="search" id="search" placeholder="Book title / Author..." />
                            <input class="submit_search" type="submit" value="" />
                        </form>
                    </div>
                </header>
                <div class="content">