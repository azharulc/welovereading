            </div><!-- /Content -->
            <footer>
                <div class="footer_wrap">
                    <div class="footer_left">
                        <p class="welove_copy">&copy; WeLoveReading.co.uk</p>
                        <?php if ($this->session->userdata('is_loggedin') && $this->session->userdata('is_admin') != 0) : ?>
                            <h5>Admin</h5>
                            <p class="admin_link"><a href="<?php echo site_url('admin'); ?>">Admin</a></p>
                        <?php endif; ?>
                    </div>
                    <div class="contact_info">
                        <h5>Contact</h5>
                        <p>Azharul Chowdhury</p>
                        <p>Email: <a href="mailto:azharul.chowdhury@my.westminster.ac.uk">azharul.chowdhury@my.westminster.ac.uk</a></p>
                    </div>
                </div>
            </footer>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
            <script src="/assets/js/plugins.js"></script>
            <script src="/assets/js/bootstrap.min.js"></script>
            <script src="/assets/js/jquery.rating.pack.js"></script>
            <script src="/assets/js/jquery.form.js"></script>
            <script src="/assets/js/main.js"></script>

            <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
            <script>
                var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
                (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
                g.src='//www.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g,s)}(document,'script'));
            </script>
        </div>
    </body>
</html>