<?php if (!empty($msg)) : ?>
	<div class="val_errors">
		<p><?php echo $msg ?></p>
	</div>
<?php endif ?>
<div class="login_wrapper">
	<div class="login_container">
		<h2>Login</h2>
		<form action="<?php echo site_url('login/process') ?>" method="post">
			<div class="field">
				<label for="username">Username</label>
				<input type="text" name="username" id="username" placeholder="Username" />
				<div class="clearfix"></div>
			</div>
			<div class="field">
				<label for="password">Password</label>
				<input type="password" name="password" id="password" placeholder="Password" />
				<div class="clearfix"></div>
			</div>
			<div class="buttons">
				<input class="button" type="submit" value="Login" />
				<a class="button" href="<?php echo site_url('registration') ?>">Register</a>
				<div class="clearfix"></div>
			</div>
		</form>
		<div class="clearfix"></div>
	</div>
</div>