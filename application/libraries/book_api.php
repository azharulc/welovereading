<?php defined('BASEPATH') OR exit('No direct script access allowed');
class book_api {
	static function search_for_book($search, $limit = null, $offset = null) {
		$url = 'http://openlibrary.org/search.json?q=' . $search;
		if (!empty($limit)) {
			$url .= '&limit=' . $limit;
		}
		if (!empty($offset) || $offset == 0) {
			$url .= '&offset=' . $offset;
		}
		$json_books = file_get_contents($url);
		return $json_books;
	}

	static function load_books_with_subject($subject, $limit = null, $offset = null) {
		$url = 'http://openlibrary.org/subjects/' . $subject . '.json?';
		if (!empty($limit)) {
			$url .= 'limit=' . $limit;
		}
		if (!empty($offset) || $offset == 0) {
			$url .= '&offset=' . $offset;
		}

		$json_books = file_get_contents($url);
		return $json_books;
	}

	static function load_book($id) {
		$url = 'http://openlibrary.org/api/books/?bibkeys=olid:' . $id . '&format=json&jscmd=data';
		$json_books = file_get_contents($url);
		return $json_books;
	}
}