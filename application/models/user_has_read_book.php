<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_has_read_book extends CI_Model {
    function __construct() {
    	$this->load->database();
        parent::__construct();
    }

    public function user_already_read_book($user_id, $book_id) {
        $result = $this->db->get_where('users_has_read_book', array('users_id' => $user_id, 'book_id' => $book_id));
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function user_read_book($user_id, $book_id, $star) {
        $data = array(
            'users_id' => $user_id,
            'book_id' => $book_id,
            'rating' => $star
        );

        $this->db->insert('users_has_read_book', $data);
    }

    public function get_user_books_details($user_id) {
        $this->db->select('ub.book_id, b.title, b.author, b.image, ub.rating')
            ->from('users_has_read_book ub')
            ->join('book b', 'ub.book_id = b.api_id')
            ->where('users_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_user_books($user_id) {
        $this->db->where('users_id', $user_id);
        $query = $this->db->get('users_has_read_book');
        return $query->result();
    }

    public function edit_rating($user_id, $book, $new_rating) {
        $place = array(
            'users_id' => $user_id,
            'book_id' => $book
        );
        $data = array(
            'rating' => $new_rating
        );

        $this->db->where($place);
        $this->db->update('users_has_read_book', $data);
    }

    public function remove_from_list($user_id, $book_id) {
        $data = array(
            'users_id' => $user_id,
            'book_id' => $book_id
        );

        $this->db->delete('users_has_read_book', $data);
    }

    public function get_all_read_books() {
        $query = $this->db->get('users_has_read_book');
        return $query->result();
    }

    public function get_user_ids() {
        $this->db->select('users_id');
        $this->db->distinct();
        $query = $this->db->get('users_has_read_book');
        return $query->result();
    }
}