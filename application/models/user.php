<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model {
    private $firstname;
    private $lastname;
    private $email;
    private $fave_gen;
    private $username;
    private $password;

    function __construct() {
    	$this->load->database();
        parent::__construct();
    }

    public function encrypt_pass($password, $name) {
        $random_string = 'c)X-;EA_b^pdY5d';
        $salt = $name . $random_string;
        return hash('sha512', $password . $salt);
    }

    public function get_name_by_username($u_name) {
        $query = $this->db->select('firstname')
            ->where('username', $u_name)
            ->get('users');
        return $query->result();
    }

    public function set_user($f_name, $l_name, $mail, $gen, $user, $pass) {
        $this->firstname = $f_name;
        $this->lastname = $l_name;
        $this->email = $mail;
        $this->fave_gen = $gen;
        $this->username = $user;
        $this->password = $this->encrypt_pass($pass, $f_name);
    }

    public function username_exists($username) {
        $result = $this->db->get_where('users', array('username' => $username));
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function email_exists($email) {
        $result = $this->db->get_where('users', array('email' => $email));
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function save_user() {
        if (!empty($this->firstname) &&
            !empty($this->lastname) &&
            !empty($this->email) &&
            !empty($this->fave_gen) &&
            !empty($this->username) &&
            !empty($this->password)) {
            $data = array(
                'firstname' => $this->firstname,
                'lastname' => $this->lastname,
                'email' => $this->email,
                'favourite_genre' => $this->fave_gen,
                'username' => $this->username,
                'password' => $this->password
            );
            $this->db->insert('users', $data);
        }
    }

    public function check_is_loggedin() {
        //checks the session to see if user is logged in
        if (!$this->session->userdata('is_loggedin')) {
            redirect('login');
        }
    }

    public function get_fave_genre($id) {
        $this->db->select('favourite_genre')
            ->where('id', $id);
        $query = $this->db->get('users');
            return $query->result();
    }

    public function validate() {
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

        if ($this->username_exists($username)) {
            $name_obj = $this->get_name_by_username($username);
            $this->firstname = $name_obj[0]->firstname;

            $encrypted_password = $this->encrypt_pass($password, $this->firstname);
            $this->db->where('username', $username);
            $this->db->where('password', $encrypted_password);
            $query = $this->db->get('users');
            if ($query->num_rows == 1) {
                $row = $query->row();
                //sets the session details
                $data = array(
                    'user_id' => $row->id,
                    'username' => $row->username,
                    'fname' => $row->firstname,
                    'lname' => $row->lastname,
                    'is_loggedin' => true,
                    'is_admin' => $row->is_admin
                );
                $this->session->set_userdata($data);
                return true;
            }
            else {
                return false;
            }
        } else {
            return false;
        }
    }
}