<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Recommendation extends CI_Model {
    function __construct() {
    	$this->load->database();
        parent::__construct();
        $this->load->model('user_has_read_book');
    }

    public function sim_person_val($user_1_books, $user_2_books) {
        //Get a list of books mutually rated by p1 and p2
        $rated_books = array();
        foreach ($user_1_books as $book1) {
            foreach ($user_2_books as $book2) {
                if ($book1->book_id == $book2->book_id) {
                    array_push($rated_books, array(
                        'user_id' => $book1->book_id,
                        'user_1_rating' => $book1->rating,
                        'user_2_rating' => $book2->rating
                    ));
                }
            }
        }
        //Find number of elements
        $num_books = count($rated_books);
        //If there are no books in common, return 0
        if ($num_books == 0) {
            return 0;
        }
        //Get the sum of all ratings
        $user_1_sum = 0;
        $user_2_sum = 0;
        foreach ($rated_books as $book) {
            $user_1_sum += $book['user_1_rating'];
            $user_2_sum += $book['user_2_rating'];
        }
        //Get the sum of the squares of the ratings
        $user_1_sq_sum = 0;
        $user_2_sq_sum = 0;
        foreach ($rated_books as $book) {
            $user_1_sq_sum += pow($book['user_1_rating'], 2);
            $user_2_sq_sum += pow($book['user_2_rating'], 2);
        }
        //Get the sum of the products of the ratings
        $sum_of_products = 0;
        foreach ($rated_books as $book) {
            $sum_of_products += ($book['user_1_rating'] * $book['user_2_rating']);
        }

        //Calculate the pearson score
        $num = $sum_of_products - ($user_1_sum * $user_2_sum / $num_books);
        $den = sqrt(($user_1_sq_sum - pow($user_1_sum, 2) / $num_books) * ($user_2_sq_sum - pow($user_2_sum, 2) / $num_books));

        if ($den == 0) {
            return 0;
        } else {
            return $num / $den;
        }
    }

    /*public function get_all_users_sim() {
        //Get current user id
        $current_user_id = $this->session->userdata('user_id');
        //Get all user ids
        $user_ids = $this->user_has_read_book->get_user_ids();
        //Get read list for current user
        $current_users_books = $this->user_has_read_book->get_user_books($current_user_id);
        //Go through all users and check for similarities and add to array
        $similarities = array();
        foreach ($user_ids as $user) {
            if ($user->users_id != $current_user_id) {
                $next_users_books = $this->user_has_read_book->get_user_books($user->users_id);
                $similarities[$user->users_id] = $this->sim_person_val($current_users_books, $next_users_books);
            }
        }
        arsort($similarities);
        //return $similarities;
        var_dump($current_users_books);
    }*/

    //Get a recommendation for a person by using a weighted average of every other user's rankings
    public function get_recommendations($books, $current_user) {
        $total = array();
        $similarity_sums = array();
        //Get current user's books
        $current_user_list = $this->user_has_read_book->get_user_books($current_user);
        foreach ($books as $book) {
            //Do not compare to self
            if ($book->users_id != $current_user) {
                //Get next users books
                $next_user_list = $this->user_has_read_book->get_user_books($book->users_id);
                //Find the similarity in the current users
                $sim = $this->sim_person_val($current_user_list, $next_user_list);
                //Ignore scores of zero or lower
                if ($sim > 0) {
                    //setup similarity arrays
                    $total[$book->book_id] = 0;
                    $similarity_sums[$book->book_id] = 0;
                    foreach ($next_user_list as $book_in_list) {
                        //Books that have not already been read
                        if ($book->book_id != $book_in_list->book_id) {
                            //Similarity * Score
                            $total[$book->book_id] += $book->rating * $sim;
                            //Sum of simailarities
                            $similarity_sums[$book->book_id] += $sim;
                        }
                    }
                }
            }
        }
        //Create the normalized list
        $rankings = array();
        foreach ($total as $code => $ttl) {
            foreach ($similarity_sums as $code2 => $s) {
                //$rankings[$total[0]] = $t / $s;
                if ($code == $code2) {
                    $rankings[$code] = ($ttl / $s);
                }
            }
        }
        return $rankings;
    }
}