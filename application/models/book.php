<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Book extends CI_Model {
    private $api_id;
    private $title;
    private $author;
    private $image;
    private $synopsis;
    private $genre;

    function __construct() {
    	$this->load->database();
        parent::__construct();
    }

    public function set_book($id, $name, $auth, $img, $synopsis = null, $genre = null) {
        $this->api_id = $id;
        $this->title = $name;
        $this->author = $auth;
        $this->image = $img;
        if (!empty($synopsis)) {
            $this->synopsis = $synopsis;
        }
        if (!empty($genre)) {
            $this->genre = $genre;
        }
    }

    public function book_exists($book_id) {
        $result = $this->db->get_where('book', array('api_id' => $book_id));
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function save_book() {
        $data = array();
        if (!empty($this->api_id) && !empty($this->title) && !empty($this->author) && !empty($this->image)) {
            $data = array(
                'api_id' => $this->api_id,
                'title' => $this->title,
                'author' => $this->author,
                'image' => $this->image
            );
        }
        if (!empty($this->synopsis)) {
            $data['synopsis'] = $this->synopsis;
        }

        if (!empty($this->genre)) {
            $data['genre'] = $this->genre;
        }

        $this->db->insert('book', $data);
    }

    public function add_review($book_id, $user_id, $title, $content) {
        $data = array(
            'title' => $title,
            'review' => $content,
            'book_id' => $book_id,
            'users_id' => $user_id
        );

        $this->db->insert('review', $data);
    }

    public function edit_review($book_id, $user_id, $title, $content) {
        $data = array(
            'title' => $title,
            'review' => $content,
        );

        $place = array(
            'book_id' => $book_id,
            'users_id' => $user_id
        );

        $this->db->where($place);
        $this->db->update('review', $data);
    }

    public function has_aready_reviewed_book($book_id, $user_id) {
        $data = array(
            'book_id' => $book_id,
            'users_id' =>$user_id
        );
        $result = $this->db->get_where('review', $data);
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_book_reviews($book_id) {
        $this->db->select('r.title, r.review, u.username, ub.rating')
            ->from('review r')
            ->join('users u', 'r.users_id = u.id', 'left')
            ->join('users_has_read_book ub', 'r.users_id = ub.users_id AND ub.book_id = r.book_id', 'inner')
            ->where('r.book_id', $book_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_user_review($book_id, $user_id) {
        $where = array(
            'book_id' => $book_id,
            'users_id' => $user_id
        );

        $this->db->select('title, review');
        $this->db->where($where);
        $query = $this->db->get('review');
        return $query->result();
    }

    public function get_subjects() {
        $query = $this->db->select('genre')
            ->get('book');
        
        return $query->result_array();
    }
}