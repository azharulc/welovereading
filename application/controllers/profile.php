<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('user');
		$this->user->check_is_loggedin();
		$this->load->model('user_has_read_book');
		$this->load->model('recommendation');
		$this->load->library('book_api');
	}

	public function index() {
		$user_id = $this->session->userdata('user_id');
		$user_books = $this->user_has_read_book->get_user_books_details($user_id);
		$data = array(
			'title' => 'Profile',
			'username' => $this->session->userdata('username'),
			'firstname' => $this->session->userdata('fname'),
			'lastname' => $this->session->userdata('lname'),
			'user_id' => $user_id,
			'user_books' => $user_books
		);
		$this->master_view('profile', $data);
	}

	public function edit_rating() {
		$user_id = $this->session->userdata('user_id');
		$book_id = $this->input->post('book_id');
		$star_key = 'stars-' . $book_id;
		$star_rating = $this->input->post($star_key);

		$this->user_has_read_book->edit_rating($user_id, $book_id, $star_rating);

		redirect('profile');
	}

	public function remove_from_list() {
		$user_id = $this->session->userdata('user_id');
		$book_id = $this->input->get('book');

		$this->user_has_read_book->remove_from_list($user_id, $book_id);
	}

	public function recommendations() {
		$books = $this->user_has_read_book->get_all_read_books();
		$user_id = $this->session->userdata('user_id');
		$recommendations = $this->recommendation->get_recommendations($books, $user_id);
		$data = array(
			'title' => 'Recommendations',
			'subtitle' => 'Here are your recommendations'
		);
		if (empty($recommendations)) {
			$fave_gen = $this->user->get_fave_genre($user_id);
			$genre = explode(', ', $fave_gen[0]->favourite_genre);
			$genre = (count($genre) > 0) ? $genre[0] : $genre;
			$book_data = book_api::load_books_with_subject($genre, 8, 0);
			$book_list = json_decode($book_data);
			$data['book_list'] = $book_list;
			$data['data_var'] = 'works';
		} else {
			$data['recommendations'] = $recommendations;
		}
		$this->master_view('listing', $data);
	}
}