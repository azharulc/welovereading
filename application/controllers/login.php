<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MY_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('user');
    }

	public function index($msg = null) {
		$data = array(
			'title' => 'Login',
			'msg' => $msg
		);
		$this->master_view('login', $data);
	}

	//Processes the login, validates the user and pass, if invalid send message, otherwise redirect to home
	public function process() {
		$result = $this->user->validate();
		if (!$result) {
			$msg = "Invalid Username / Password.";
            $this->index($msg);
		} else {
			redirect('/');
		}
	}

	//destroys session and redirect to home
    public function logout() {
        $this->session->sess_destroy();
        redirect('/');
    }
}