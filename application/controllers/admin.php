<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('user');
		$this->user->check_is_loggedin();
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	
	}

	function _example_output($output = null)
	{
		$this->load->view('admin.php',$output);	
	}

	function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}	
	
	function manage_reviews()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_table('review');
			$crud->set_subject('Review');

			$crud->set_relation('users_id', 'users', '{firstname} {lastname} ({username})');
			
			$output = $crud->render();
			
			$this->_example_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function manage_users()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_table('users');
			$crud->set_subject('Users');

			$crud->columns('id','firstname','lastname','email','favourite_genre', 'username', 'is_admin');
			
			$output = $crud->render();
			
			$this->_example_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}