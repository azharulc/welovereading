<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Registration extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('user');
	}

	public function index() {
		$data = array('title' => 'Registration');
		$this->master_view('registration', $data);
	}

	//Registration + rules
	public function register() {
		$this->form_validation->set_rules('firstname', 'First Name', 'required|alpha');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('fave_gen', 'Favourite Genre', 'required|alpha');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('conf_password', 'Confirmation Password', 'required|matches[password]');

		if ($this->form_validation->run() == false) {
			$data = array('title' => 'Registration');
			$this->master_view('registration', $data);
		} else {
				$firstname = $this->input->post('firstname');
				$lastname = $this->input->post('lastname');
				$email = $this->input->post('email');
				$fave_gen = $this->input->post('fave_gen');
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$conf_password = $this->input->post('conf_password');

				$this->user->set_user($firstname, $lastname, $email, $fave_gen, $username, $password, $conf_password);
			//check if username exists in system
			if ($this->user->username_exists($username)) {
				$data = array(
					'title' => 'Registration',
					'error' => 'username_exists'
				);
				$this->master_view('registration', $data);
			} else {
				//Check if email exists in system
				if ($this->user->email_exists($email)) {
					$data = array(
						'title' => 'Registration',
						'error' => 'email_exists'
					);

					$this->master_view('registration', $data);
				} else {
					$this->user->save_user();

					$data = array('title' => 'Registration Success');
					$this->master_view('registration_success', $data);
				}
			}
		}
	}
}