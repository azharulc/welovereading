<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('book_api');
		$this->load->model('book');
	}

	public function index() {
		$subject = 'fantasy';

		$book_data = book_api::load_books_with_subject($subject, 8, 0);
		$book_list = json_decode($book_data);

		$data = array(
			'title' => 'Home',
			'subtitle' => $subject . ' ' . 'books',
			'book_list' => $book_list,
			'data_var' => 'works'
		);

		$this->master_view('listing', $data);
	}

	public function get_random_genre() {
		$genres_array = $this->book->get_subjects();
		$genres_index = array_rand($this->book->get_subjects());
		$genres = $genres_array[$genres_index];
		$genre_array = explode(', ', $genres['genre']);
		$genre_index = array_rand($genre_array);
		$genre = $genre_array[$genre_index];

		return $genre;
	}

	public function search() {
		$raw_search = $this->input->get('search');
		$search = str_replace(' ', '+', $raw_search);

		$book_data = book_api::search_for_book($search, 8, 0);
		$book_list = json_decode($book_data);

		$data = array(
			'title' => 'Search',
			'subtitle' => 'You have searched for "' . $raw_search . '"',
			'book_list' => $book_list,
			'data_var' => 'docs'
		);
		$this->master_view('listing', $data);
	}
}