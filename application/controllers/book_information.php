<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Book_information extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('book');
		$this->load->model('user_has_read_book');
		$this->load->library('book_api');
		$this->load->library('form_validation');
	}

	private function get_index_data($book_id) {
		$user_id = $this->session->userdata('user_id');
		$book_info = json_decode(book_api::load_book($book_id));

		$user_review = null;
		if ($this->book->has_aready_reviewed_book($book_id, $user_id)) {
			$user_review = $this->book->get_user_review($book_id, $user_id);
		}
		$data = array(
			'title' => 'Book Information',
			'book_id' => $book_id,
			'book_info' => $book_info,
			'has_read_book' => $this->user_has_read_book->user_already_read_book($user_id, $book_id),
			'reviews' => $this->book->get_book_reviews($book_id),
			'user_review' => $user_review
		);

		return $data;
	}

	public function index() {
		$book_id = $this->input->get('book');
		$data = $this->get_index_data($book_id);
		$this->master_view('book_info', $data);
	}

	public function save() {
		$api_id = $this->input->post('book_id');
		$rating = $this->input->post('stars');

		if ($this->book->book_exists($api_id)) { 
			//Do nothing
		} else {
			$title = $this->input->post('title');
			$author = $this->input->post('author');
			$image = $this->input->post('img');
			$subject = $this->input->post('subject');
			$subtitle = $this->input->post('subtitle');

			$this->book->set_book($api_id, $title, $author, $image, $subtitle, $subject);
			$this->book->save_book();
		}

		$user_id = $this->session->userdata('user_id');
		if ($this->user_has_read_book->user_already_read_book($user_id, $api_id)) {
            // do nothing
        } else {
			$this->user_has_read_book->user_read_book($user_id, $api_id, $rating);
		}

		redirect('profile');
	}

	public function leave_review() {
		$book_id = $this->input->post('book_id');
		$user_id = $this->session->userdata('user_id');
		$review_title = $this->input->post('review_title');
		$review = $this->input->post('review_body_box');

		$this->form_validation->set_rules('review_title', 'Review Title', 'required');
		$this->form_validation->set_rules('review_body_box', 'Review', 'required');

		if ($this->form_validation->run() == false) {
			$data = $this->get_index_data($book_id);
			$this->master_view('book_info', $data);
		} else {
			if (!$this->book->has_aready_reviewed_book($book_id, $user_id)) {
				$this->book->add_review($book_id, $user_id, $review_title, $review);
			} else {
				$this->book->edit_review($book_id, $user_id, $review_title, $review);
			}
			$data = $this->get_index_data($book_id);
			$this->master_view('book_info', $data);
		}
	}
}