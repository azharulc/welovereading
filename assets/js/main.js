$(document).ready(function() {
	/*******************************************************************
		IPhone
	*******************************************************************/
	//Toggle nav on iphone
	$('.show_menu').click(function(e) {
		e.preventDefault();
		$('header nav').slideToggle();
	});

	/*******************************************************************
		Bigger Screens
	*******************************************************************/
	//Remove styling on larger screens
	$(window).resize(function() {
		if ($(window).width() > 480){
			$('header nav').removeAttr('style');
		}
	});

	/**** Book Information Page ****/
	$('.nav-tabs li a').click( function(e) {
 		e.preventDefault();
  		$(this).tab('show');
	});

	$('.nav-tabs li a[href="#review"]').tab('show');

	/**** Profile Page ****/
	$('.book_display .stars .button, .book_display .stars .rating-cancel').addClass('hidden');

	$('.stars form .star-rating a').click(function(e) {
		e.preventDefault();
		//find current row book id and rating
		var bookid = $(this).parent().parent().parent().parent().find('.book_id').val();
		var rating = $(this).html();
		//create post name for correct rating
		var rating_name = 'stars-' + bookid;

		data = {
			book_id: bookid
		}
		data[rating_name] = rating;

		$.post('profile/edit_rating', data);
	});

	$('.to_do .remove').click(function(e) {
		e.preventDefault();
		var book_row = $(this).parent().parent()
		var bookid = book_row.find('.book_id').val();
		book_row.slideUp("slow", function(){
			$(this).remove();
		});

		$.get('profile/remove_from_list', {book: bookid});
	});
});
